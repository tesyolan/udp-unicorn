# README #

Inital attempt at giving UDP Unicorn a command line interface. 

### How do I get set up? ###

* Install codeblocks and Cygwin
* Open the codeblocks project file. 
* Dependencies
gdi32
winmm
comctl32
comdlg32
iphlpapi
user32
-lws2_32
* How to run
I have run the system by setting the the command line parameters in the codeblocks or via from the bin/debug/UDPUnicorn2.exe. 
* Running instructions.
UDPUnicorn2.exe ip port size delay thread_no socker_per_thread. 

Example:
UDPUnicorn2.exe 192.168.1.24 HTTP 10 10 10 10


### Remaining Parts ###
* Writing Command Line helper functions to check whether the parameters are valid
* Using either console window handler or completely removing all the hwnd related commands and trimming the code to the bare minium of the desired functionality to either enter the state via the main or wmain functions. Also remove after having removed the unnecessary windows related functions trim the linked libraries to all the relvant ones for the socket connection ones. 
* ReConfirm to the guidelines in the google docs. 
* Make it work on the release version.
* Handle DNS and Pinging of target to check for viablility of the target. 
* Start the Repo from the fork of the sourceforge library; and add files additionally. As this is a copy of a library after having made multiple changes to the original libraries to make it compile in the Cygwin library.